//
//  ColorsVC.swift
//  Becki project
//
//  Created by Maks Vogtman on 24/09/2021.
//

import UIKit

class ColorsVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
    }
    
}
