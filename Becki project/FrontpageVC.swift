//
//  FrontpageVC.swift
//  Becki project
//
//  Created by Maks Vogtman on 24/09/2021.
//

import UIKit

class FrontpageVC: UIViewController {
    
    // MARK: - Declaration
    fileprivate lazy var label: UILabel = {
        let label = UILabel()
        label.text = "Please say the amount of colors"
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)
        label.font = .systemFont(ofSize: 20, weight: .bold)
        label.tintColor = .label
        label.backgroundColor = .systemBackground
        return label
    }()
    
    fileprivate lazy var textField: UITextField = {
       let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(textField)
        textField.placeholder = "Type number"
        textField.layer.cornerRadius = 12
        textField.layer.borderWidth = 1
        textField.backgroundColor = .systemBackground
        return textField
    }()
    
    fileprivate lazy var button: UIButton = {
       let button = UIButton()
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(button)
        button.setTitle("Next", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.backgroundColor = .secondarySystemBackground
        return button
    }()
    
    @objc private func buttonTapped() {
        navigationController?.pushViewController(ColorsVC(), animated: true)
    }
    
    
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        title = "kolorki wszechświata"
        navigationController?.navigationBar.prefersLargeTitles = true
        layoutConstraints()
    }
    
    fileprivate func layoutConstraints() {
        NSLayoutConstraint.activate([
            textField.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            textField.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            textField.widthAnchor.constraint(equalToConstant: 300),
            textField.heightAnchor.constraint(equalToConstant: 50),
        
            label.centerXAnchor.constraint(equalTo: textField.centerXAnchor),
            label.bottomAnchor.constraint(equalTo: textField.topAnchor, constant: -100),
        
            button.centerXAnchor.constraint(equalTo: textField.centerXAnchor),
            button.topAnchor.constraint(equalTo: textField.bottomAnchor, constant: 100),
            button.widthAnchor.constraint(equalToConstant: 250),
            button.heightAnchor.constraint(equalToConstant: 50)
        ])
        
    }


}
